<?php

    //header('Content-type: application/json');

    include("connection.php");

    $sql = "SELECT * FROM versos";

    $result = $conn->query($sql);

    $json = array();

    while($row = $result->fetch_assoc()){

        $array['capitulo'] = $row['capitulo'];
        $array['verso'] = $row['verso'];
        $array['traduccion'] = utf8_encode($row['traduccion']);
        array_push($json, $array);
       
    }
    echo json_encode($json);

?>