function agregarDatos(capitulo, verso, traduccion, significado){
    
    var valores =   "capitulo=" + capitulo +
                "&verso=" + verso +
                "&traduccion=" + traduccion +
                "&significado=" + significado;
    $.ajax({
        type:'POST',
        url:'insert.php',
        data:{
            capitulo:capitulo,
            verso: verso,
            traduccion: traduccion,
            significado: significado
        },
        success:function(data){
            
                $('#datos').load('insert.php');
        }
    });
}
