<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
          crossorigin="anonymous">
    <link rel="stylesheet" href="bootstrap/css/bootstrap.css">
    <script src="jquery.js" charset="utf-8"></script>
    <script src="https://ajax.aspnetcdn.com/ajax/jQuery/jquery-3.4.1.js"></script>
    <script src="notify.js" charset="utf-8"></script>

    <script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
    <script>
         (adsbygoogle = window.adsbygoogle || []).push({
              google_ad_client: "ca-pub-6028288480509132",
              enable_page_level_ads: true
         });
    </script>

    <title>Gita System</title>
  </head>
  <body>
    <div class="container">

    <?php
      include "navbar.php";

      if (isset($_GET['action'])) {
        $action = $_GET['action'];
      }else{
        $action = "inicio";
      }

      switch($action){
        case 'inicio':
          include "home.php";
        break;

        case 'subir':
          include "subir.php";
        break;

        case 'leer':
          include "leer.php";
        break;

        case 'capitulo':
          include "capitulo.php";
        break;

        case 'significado':
          include "significado.php";
        break;

        case 'documentacion':
          include 'documentacion.php';
        break;

        default:
          // code...
        break;
      }

     ?>
   </div>

  </body>
</html>
<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

<script type='text/javascript'>
//<![CDATA[
// Botón para Ir Arriba
jQuery.noConflict();
jQuery(document).ready(function() {
jQuery("#IrArriba").hide();
jQuery(function () {
jQuery(window).scroll(function () {
if (jQuery(this).scrollTop() > 200) {
jQuery('#IrArriba').fadeIn();
} else {
jQuery('#IrArriba').fadeOut();
}
});
jQuery('#IrArriba a').click(function () {
jQuery('body,html').animate({
scrollTop: 0
}, 800);
return false;
});
});


});
//]]>
</script>
<script src="jquery.js" charset="utf-8"></script>
<script>
    $(function(){
        var URLsearch = window.location.search;
        //alert(URLsearch);

        $('.navbar-nav li').removeClass('active');
        //$(this).parent().addClass('active');

        var a = $("li > a").attr("href");
        //alert(a);

        $('a[href="'+ URLsearch +'"]').addClass('active');

        if (URLsearch < 1) {
          $('a[href="?action=home"]').addClass('active');
        }
    });
</script>
