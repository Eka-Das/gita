<nav class="navbar navbar-expand-lg navbar-light bg-light">
  <a class="navbar-brand" href="#">El Bhagavad-Gita: Tal Como Es</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarNav">
    <ul class="navbar-nav">
      <li class="nav-item active">
        <a class="nav-link" href="?action=inicio">Inicio <span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="?action=leer" tabindex="-1" aria-disabled="true">Leer Versos</a>
      </li>
      <!--<li class="nav-item">
        <a class="nav-link" href="?action=subir">Subir Versos</a>
      </li>-->
      <li class="nav-item">
        <a class="nav-link" href="?action=documentacion">Documentación</a>
      </li>
      <!--<li class="nav-item">
        <a class="nav-link" href="">Pricing</a>
      </li>-->

    </ul>
    <ul class="nav navbar-nav navbar-right">
      <span class="navbar-text" style="right: 0px; position: absolute;">
        V 1.0
      </span>
      <!-- … -->
    </ul>
  </div>
</nav>
