-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost
-- Tiempo de generación: 06-07-2019 a las 02:56:59
-- Versión del servidor: 10.1.37-MariaDB
-- Versión de PHP: 7.3.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `gita`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `capitulos`
--

CREATE TABLE `capitulos` (
  `id_capitulo` int(11) NOT NULL,
  `capitulo` varchar(80) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `capitulos`
--

INSERT INTO `capitulos` (`id_capitulo`, `capitulo`) VALUES
(1, 'Observando los ejÃ©rcitos en el campo de batalla de Kuruksetra'),
(2, 'Resumen del contenido del Gita'),
(3, 'Karma-yoga'),
(4, 'El conocimiento trascendental'),
(5, 'Karma-yoga: Accion con conciencia de Krsna'),
(6, 'Dhyana-yoga'),
(7, 'El conocimiento del Absoluto'),
(8, 'Alcanzando al Supremo'),
(9, 'El conocimiento más confidencial'),
(10, 'La opulencia del Absoluto'),
(11, 'La forma universal'),
(12, 'El servicio devocional'),
(13, 'La naturaleza, el disfrutador y la conciencia'),
(14, 'Las tres modalidades de la naturaleza material'),
(15, 'El yoga de la Persona Suprema'),
(16, 'La naturaleza divina y la demonÃ­aca'),
(17, 'Las divisiones de la fe'),
(18, 'ConclusiÃ³n: La perfecciÃ³n de la renunciaciÃ³n');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `versos`
--

CREATE TABLE `versos` (
  `id` int(11) NOT NULL,
  `capitulo` int(11) NOT NULL,
  `verso` int(11) NOT NULL,
  `traduccion` text COLLATE utf8_spanish_ci NOT NULL,
  `significado` mediumtext COLLATE utf8_spanish_ci NOT NULL,
  `agregado` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `versos`
--

INSERT INTO `versos` (`id`, `capitulo`, `verso`, `traduccion`, `significado`, `agregado`) VALUES
(1, 1, 1, 'Dhrtarastra dijo: ¡Oh, Sañjaya!, ¿qué hicieron mis hijos y los hijos de Pandu después de reunirse en el lugar de peregrinaje de Kuruksetra con deseos de pelear?', 'El Bhagavad-gita es la muy leída ciencia teísta que se resume en El Gita-mahatmya (la glorificación del Gita). Allí se dice que uno debe leer El Bhagavad-gita muy detenidamente, con la ayuda de alguien que sea devoto de Sri Krsna, y tratar de entenderlo sin interpretaciones motivadas por intereses personales. El ejemplo de una clara comprensión se encuentra en el mismo Bhagavad-gita, en la forma en que la enseñanza fue entendida por Arjuna, quien oyó el Gita directamente de labios del Señor. Si alguien es lo suficientemente afortunado como para entender El Bhagavad-gita en esa línea de sucesión discipular, sin una interpretación producto de motivaciones personales, supera entonces todos los estudios de la sabiduría védica y de todas las Escrituras del mundo. Uno encontrará en El Bhagavad- gita todo lo que contienen las demás Escrituras, pero el lector también encontrará cosas que no se han de encontrar en ninguna otra parte. Ésa es la pauta específica del Gita. El Bhagavad-gita es la ciencia teísta perfecta, porque lo habla directamente la Suprema Personalidad de Dios, el Señor Krsna.\r\n\r\nLos temas que discuten Dhrtarastra y Sañjaya, tal como se describen en El Mahabharata, constituyen el principio básico de esta gran filosofía. Se sabe que esta filosofía se desarrolló en el campo de batalla de Kuruksetra, que es un lugar sagrado de peregrinaje desde los tiempos inmemoriales de la época védica. Con el fin de guiar a la humanidad, el Señor la expuso mientras se hallaba presente personalmente en este planeta.\r\n\r\nEn la actualidad, el hombre está adelantado en lo que respecta a la ciencia material, pero hasta ahora no ha logrado conseguir la unidad de toda la raza humana que hay en la faz de la Tierra. El Bhagavad-gita va a solucionar ese problema, ya que los hombres inteligentes encontrarán en esta gran obra teísta la unidad de toda la sociedad humana. Mediante el estudio a fondo de El Bhagavad-gita es absolutamente posible que en el mundo entero haya sólo una Escritura, es decir, El Bhagavad-gita, un solo Dios, el Señor Krsna, el hijo de Devaki, y que toda la raza humana cante un solo himno: la glorificación del santo nombre del Señor Krsna. Ese canto del santo nombre del Señor Krsna lo recomendó mucho el propio Señor, y la gente está sintiendo sus efectos en la práctica mediante el canto de Hare Krsna, Hare Krsna, Krsna Krsna, Hare Hare/ Hare Rama, Hare Rama, Rama Rama, Hare Hare. En el mundo occidental ya ha comenzado el canto de ese glorioso santo nombre, introducido por la Sociedad Internacional para la Conciencia de Krsna, y el mismo gradualmente se está difundiendo por todas partes del mundo, para que la raza humana pueda tener sólo una religión, un Dios, un himno y una ocupación, en el servicio del Señor. Eso hará aparecer la paz que tan ansiosamente se desea en el mundo.\r\n\r\nLa palabra dharma-ksetra (un lugar en el que se celebran rituales religiosos) es significativa, porque en el campo de batalla de Kuruksetra, la Suprema Personalidad de Dios se encontraba presente del lado de Arjuna. Dhrtarastra, el padre de los Kurus, dudaba mucho de que sus hijos lograran la victoria final. En medio de la duda, le preguntó a Sañjaya, su secretario: \"¿Qué han hecho mis hijos y los hijos de Pandu?\". Él estaba seguro de que tanto sus hijos como los hijos de Pandu, su hermano menor, estaban reunidos en ese campo de Kuruksetra, decididos a consumar la guerra. Sin embargo, su pregunta es significativa. Él no quería que hubiera un arreglo entre los primos y hermanos, y quería estar seguro de la suerte que correrían sus hijos en el campo de batalla. Como se había dispuesto que la batalla se librara en Kuruksetra, que en otra parte de los Vedas se designa como un lugar de adoración, incluso para los ciudadanos del cielo, Dhrtarastra sintió mucho temor de la influencia que el sagrado lugar podía tener en el desenlace de la batalla. Él sabía muy bien que ello tendría una influencia favorable en Arjuna y los hijos de Pandu, porque éstos eran todos virtuosos por naturaleza. Sañjaya era un alumno de Vyasa, y, en consecuencia, por la misericordia de Vyasa, podía ver el campo de batalla de Kuruksetra aun mientras se encontraba en el aposento de Dhrtarastra. Y, por eso, Dhrtarastra le preguntó cuál era la situación en el campo de batalla.\r\n\r\nTanto los Pandavas como los hijos de Dhrtarastra pertenecen a la misma familia, pero aquí queda al descubierto lo que Dhrtarastra estaba pensando. Él deliberadamente reconoció como Kurus sólo a sus hijos, y apartó a los hijos de Pandu del patrimonio de la familia. Con esto, uno puede entender la posición específica de Dhrtarastra en relación con sus sobrinos, los hijos de Pandu. Así como en el arrozal se arrancan las plantas innecesarias, así mismo se esperaba desde el propio comienzo de estos asuntos, que, en el campo religioso de Kuruksetra, en el que se hallaba presente el padre de la religión, Sri Krsna, se extirparían las malas hierbas, tales como Duryodhana, el hijo de Dhrtarastra, y los demás, y que el Señor les daría el poder a las personas enteramente religiosas, encabezadas por Yudhisthira. Ése es el significado de las palabras dharma-ksetre y kuru-ksetre, aparte de su importancia védica e histórica.', '2019-07-01');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `capitulos`
--
ALTER TABLE `capitulos`
  ADD PRIMARY KEY (`id_capitulo`);

--
-- Indices de la tabla `versos`
--
ALTER TABLE `versos`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `capitulos`
--
ALTER TABLE `capitulos`
  MODIFY `id_capitulo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT de la tabla `versos`
--
ALTER TABLE `versos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
