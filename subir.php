<div class="container">
  <div class="row">
    <div class="col-sm-12 col-lg-6 offset-lg-3 col-md-12">

      <h3 class="text-center">Sube un nuevo verso</h3>
      <div class="form-group">
        <label for=""># Del Capitulo</label>
        <input  type="number" id="capitulo" class="form-control"
        placeholder="" aria-describedby="helpId">
        <small id="helpId" class="text-muted">Del 1 al 18</small>
      </div>
      <div class="form-group">
        <label for="">Verso</label>
        <input  type="text" name="" id="verso" class="form-control"
        placeholder="Ejemplo: '21' o '21-22'(sin comillas) " aria-describedby="helpId" min="">
        <small id="helpId" class="text-muted">Puedes añadir versos separados por un guion: 21-22</small>
      </div>
      <div class="form-group">
        <label for="my-textarea">Traduccion</label>
        <textarea id="traduccion" class="form-control"
        name="" rows="3"></textarea>
      </div>
      <div class="form-group">
        <label for="my-textarea">Significado</label>
        <textarea id="significado" class="form-control"
        name="" rows="3"></textarea>
      </div>
      <button type="buttons" class="btn btn-primary" id="button">Submit</button>

    </div>
  </div>
</div>
<p id="response"></p>
<script type="text/javascript">
  $(function(){
    $("#verso").change(function(){
      var verso = $("#verso").val();
      var capitulo = $("#capitulo").val();
      //alert("capitulo: " + capitulo + " verso: " +verso);

      $.ajax({
        url: 'compara.php',
        method: 'POST',
        //datatype:'json',
        data:{capitulo:capitulo, verso:verso},
        success: function(data){
          //alert(data);
          if (data=="true") {
            //alert("si");
            $("#capitulo").notify("OK",{position:"right top", className:"success"});
            $("#verso").notify("Puedes Continuar",{position:"right top", className:"success"});
          }else if (data=="false") {
            $("#capitulo").notify("error",{position:"right top", className:"error"});
            $("#verso").notify("ya está registrado este verso",{position:"right top", className:"error"});
          }
          //$('#response').text(data);
        }
      });
    });
  });
</script>



    <script>
        $(function(){
            $("#button").click(function(){
                var capitulo = $("#capitulo").val();
                var verso=$("#verso").val();
                var traduccion=$("#traduccion").val();
                var significado=$("#significado").val();
                $.ajax({
                    url:'insert.php',
                    method:'POST',
                    data:{
                        capitulo:capitulo,
                        verso:verso,
                        traduccion:traduccion,
                        significado:significado
                    },
                   success:function(data){
                       alert(data);
                   }
                });
            });
        });
    </script>
