<style media="screen">
/* Botón Ir Arriba
----------------------------------------------- */
#IrArriba {
position: fixed;
bottom: 30px; /* Distancia desde abajo */
right: 30px; /* Distancia desde la derecha */
}

#IrArriba span {
width: 60px; /* Ancho del botón */
height: 60px; /* Alto del botón */
display: block;
background: url(http://lh5.googleusercontent.com/-luDGEoQ_WZE/T1Ak-gta5MI/AAAAAAAACPI/ojfEGiaNmGw/s60/flecha-arriba.png) no-repeat center center;
}
</style>

<h3>Ver Capítulos</h3>

<!--ver todos los capítulos
  ver todos los versos de todos los capítulos

elegir el capítulo
  ver todos los versos del capítulo seleccionado
  ver solo un verso seleccionado


capitulos->muestra versos
-->
<div class="row">
  <!--<div class="col-sm-2">

    <nav class="nav flex-column nav-pills">
      <a class="nav-link disabled" href="#" tabindex="-1" aria-disabled="true">Capítulos</a>

      <a class="nav-link active" href="#">Capítulo 1</a>
      <a class="nav-link" href="#">Capítulo 2</a>
      <a class="nav-link" href="#">Capítulo 3</a>
      <a class="nav-link" href="#">Capítulo 4</a>
      <a class="nav-link" href="#">Capítulo 5</a>
      <a class="nav-link disabled" href="#" tabindex="-1" aria-disabled="true">Disabled</a>
    </nav>-->


    <!--
      <div class="col-sm-6">
        <div class="card">
          <div class="card-body">
            <h5 class="card-title">Special title treatment</h5>
            <p class="card-text">With supporting text below as a natural lead-in to additional content.</p>
            <a href="#" class="btn btn-primary">Go somewhere</a>
          </div>
        </div>
      </div>
    -->
    <?php

      include "connection.php";

      //if(isset($_POST['capitulo']) & isset($_POST['verso'])){

      /*$capitulo = $_POST['capitulo'];
      $verso    = $_POST['verso'];*/

      //echo $capitulo . $verso;


      //echo "el número de verso es: " . $verso;
      $sql = "SELECT * FROM capitulos";
      $result = $conn->query($sql);

      if($result -> num_rows > 0){

        while($row = $result->fetch_assoc()){
          ?>
            <div class="col-sm-6">
              <div class="card">
                <div class="card-body">
                  <h5 class="card-title"><b>Capítulo <?php echo $row['id_capitulo'];?></b></h5>
                  <p class="card-text"><?php echo $row['capitulo'];  ?></p>
                  <a href="?action=capitulo&id_capitulo=<?php echo $row['id_capitulo']; ?>" class="btn btn-primary">Ver Versos</a>
                </div>
              </div>
            </div>
          <?php
        }
        /*
          $data = array("capitulo" => $capitulo, "verso" => $verso);
          $json = json_encode($data);
          echo "ya existe";
          echo $json;
        */
        //  echo "false";
        //  }
      }
    ?>
  </div>
  <div id='IrArriba'>
  <a href='#Arriba'><span/></a>
  </div>
