<?php

  include "connection.php";
  if (isset($_GET['id_capitulo'])) {
    $id_capitulo = $_GET['id_capitulo'];

    $sql = "SELECT * FROM versos WHERE capitulo=".$id_capitulo;
    $result = $conn->query($sql);
?>
<div class="row">
  <div class="col-sm-12 col-md-6 offset-md-3">
<h3><b>Capítulo <?php echo $id_capitulo; ?></b></h3>

<?php
  if($result -> num_rows > 0){

    while($row = $result->fetch_assoc()){
      ?>
        <div class="card">
          <h5 class="card-header"><b>Verso <?php echo $row['verso']; ?></b></h5>
          <div class="card-body">
            <!--<h5 class="card-title"><?php //echo $row['significado']; ?></h5>-->
            <p class="card-text"><?php echo $row['traduccion'];?></p>
            <a href="?action=significado&id=<?php echo $row['id']; ?>" class="btn btn-primary">Significado</a>
          </div>
        </div>
      <?php

    }
  }
}

      ?>

    </div>

  </div>
