<?php
    include_once 'pelicula.php';

    class apiPeliculas{

        function getAll{
            $pelicula = new pelicula();
            $pelicula = array();
            $peliculas["items"] = array();

            $res = $pelicula->obtenerPeliculas();

            if($res->rowCount()){
                while($row = $res->fetch(PDO::FETCH_ASSOC)){
                    $item = array(
                        'id' => $row['id'],
                        'nombre' => $row['nombre'],
                        'imagen' => $row['imagen']
                    );
                    array_push($peliculas['items'], $item);
                }
            }else{
                echo json_encode(array('mensaje' => 'no hay elementos registrados'));
            }
        }
    }